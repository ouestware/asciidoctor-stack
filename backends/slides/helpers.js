module.exports.configure = context => {
  // Default values
  context.nunjucks.environment.addGlobal("DEFAULT_ENCODING", "UTF-8");
  context.nunjucks.environment.addGlobal("DEFAULT_LANG", "en");
  context.nunjucks.environment.addGlobal("DEFAULT_TOC_LEVELS", 2);
  context.nunjucks.environment.addGlobal("DEFAULT_TOC_PLACEMENT", "auto");
  context.nunjucks.environment.addGlobal("DEFAULT_TOC_CLASS", "toc");
  context.nunjucks.environment.addGlobal("DEFAULT_LABEL_VERSION", "Version");
  context.nunjucks.environment.addGlobal("DEFAULT_LABEL_LAST_UPDATE", "Last updated");

  /**
   * Retrieve the numeral path of the section.
   *
   * @param {Section} block
   * @returns {Array<string>}
   */
  function getSectionNumeralPath(section) {
    let numerals = [];
    numerals.push(section.getNumeral());
    let parent = section.getParent();
    while (parent && parent.getNumeral() && typeof parent.getNumeral() === "string") {
      numerals.push(parent.getNumeral());
      parent = parent.getParent();
    }
    return numerals.reverse();
  }
  context.nunjucks.environment.addGlobal("getSectionNumeralPath", getSectionNumeralPath);

  /**
   * Generate recursively the toc ul/li.
   *
   * @param {Section} block
   * @param {number} maxLevel The maximum level of the TOC
   * @return {string} The html of the toc
   */
  function generateToc(block, maxLevel) {
    let html = "";

    if (block.getLevel() <= maxLevel && block.getSections().length > 0) {
      html += `<ul class="sectlevel${block.getLevel() + 1}">`;
      block.getSections().forEach((section, index) => {
        html += `<li>`;
        html += ` <a href="#${section.getId()}">${getSectionNumeralPath(section).join(".")}. ${section.getTitle()}</a>`;
        html += generateToc(section, maxLevel);
        html += `</li>`;
      });
      html += `</ul>`;
    }
    return html;
  }
  context.nunjucks.environment.addGlobal("generateToc", generateToc);

  /**
   * Compute the hihlighter config for the `block_listing`
   *
   * @param {Section} node
   * @param {string} highlighter The highligther name
   * @return {object}
   */
  function getHighlighterConfig(node, highlighter, nowrap) {
    let config = {
      code_lang: node.getAttribute("language"),
      code_class: node.getAttribute("language") ? [`language-${node.getAttribute("language")}`] : undefined,
      pre_class: ["highlight"],
      pre_lang: undefined,
    };

    switch (highlighter) {
      case "coderay":
        config.pre_class.push("CodeRay");
        config.code_class = undefined;
        break;
      case "pygments":
        config.pre_class.push("pygments");
        config.code_class = undefined;
        break;
      case "highlight.js":
      case "highlightjs":
        config.pre_class.push("highlightjs");
        break;
      case "prettify":
        config.pre_class.push("prettyprint");
        if (node.getAttribute("linenums")) {
          config.pre_class.push("linenums");
        }
        break;
      case "html-pipeline":
        config.pre_lang = node.getAttribute("language");
        config.code_class = node.getAttribute("language");
        nowrap = false;
        break;
      case "codemirror":
        config.source_lang = node.getAttribute("language");
        config.code_class = node.getAttribute("language");
        break;
    }
    if (nowrap) {
      config.pre_class.push("nowrap");
    }
    return config;
  }
  context.nunjucks.environment.addGlobal("getHighlighterConfig", getHighlighterConfig);

  // Custom Nunjucks filter
  context.nunjucks.environment.addFilter("toBool", str => str.toString() === "true");
  context.nunjucks.environment.addFilter("toInt", str =>
    str !== "Infinity" ? parseInt(str) : Number.POSITIVE_INFINITY,
  );

  /**
   * Given an admonition nae, this method returns the adequate FA icon name and color.
   *
   * @param {string} name of hte admonition
   * @return {object} icon name + color
   */
  function admonitionNameToFontAwesome(name) {
    switch (name.toLowerCase()) {
      case "tip":
        return { name: "lightbulb", color: "yellow" };
      case "important":
        return { name: "exclamation-circle", color: "red" };
      case "warning":
        return { name: "exclamation-triangle", color: "orange" };
      case "caution":
        return { name: "exclamation-circle", color: "blue" };
      default:
        return { name: "info-circle", color: "blue" };
    }
  }
  context.nunjucks.environment.addGlobal("admonitionNameToFontAwesome", admonitionNameToFontAwesome);
};
