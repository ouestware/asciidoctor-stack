import * as bs from "browser-sync";
import * as path from "path";
import { asciidoctor, Option } from "./asciidoctor";
import { scss } from "./stylesheet";
import { getOption } from "./utils";

const asciidoctorServer = async (
  root: string,
  backendPath: string,
  options: Array<Option>,
  callback?: (err, server) => void,
): Promise<void> => {
  // Create the browerser sync server
  const bsServer = bs.create();

  // Compute some variables
  // ~~~~~~~~~~~~~~~~~~~~~~
  const rootPath: string = path.dirname(root);
  const htmlFile = path.basename(root).replace(/.adoc$/i, ".html");
  const stylesdir: string = getOption<string>(options, "stylesdir", path.resolve(__dirname + "./../../themes/"));
  const stylesheet: string = getOption<string>(options, "stylesheet", "index.css");

  // Generateadoc options for server mode
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // retrieve directory options
  const adocDirOptions = options.filter(option => option.name.endsWith("dir"));
  // all options that are not directories
  const adocOption: Array<Option> = options
    .filter(option => !option.name.endsWith("dir"))
    .concat(
      adocDirOptions.map(option => {
        return { name: option.name, value: `/${option.name.replace(/-?dir/, "")}` };
      }),
    );

  // Register watcher
  // ~~~~~~~~~~~~~~~~~~~~~
  // Watcher for adoc files : generate html file from adoc
  bsServer.watch(`${rootPath}/**/*.adoc`, async (event: string, file: string) => {
    if ((event === "change" || event === "add") && file.endsWith(".adoc")) {
      console.log(`File ${file} has been ${event}, regenerate ${root} file`);
      try {
        await asciidoctor(root, backendPath, adocOption);
      } catch (e) {
        console.log("Failed to generate adoc file", e);
      }
    }
  });
  // Watcher for scss : compile to css
  bsServer.watch(`${stylesdir}/**/*.scss`, async (event: string, file: string) => {
    if (event === "change" || event === "add") {
      console.log(`File ${file} has been ${event}, regenerate ${stylesdir}/index.scss file`);
      try {
        await scss(`${stylesdir}/${stylesheet.replace(/.css$/i, ".scss")}`);
      } catch (e) {
        console.log("Failed to generate SCSS file", e);
      }
    }
  });
  // Watcher for html : reload
  bsServer.watch(htmlFile, () => {
    bsServer.reload(htmlFile);
  });
  // Watcher for css : reload
  bsServer.watch(`${stylesdir}/${stylesheet}`, () => {
    bsServer.reload(`${stylesdir}/${stylesheet}`);
  });

  // Launch the server
  //~~~~~~~~~~~~~~~~~~
  bsServer.init(
    {
      server: path.dirname(root),
      serveStatic: adocDirOptions.map(option => {
        return { route: `/${option.name.replace(/-?dir/, "")}`, dir: option.value };
      }),
      injectChanges: true,
      watchEvents: ["change", "add"],
      startPath: path.basename(root).replace(/.adoc$/i, ".html"),
      browser: callback ? [] : "default",
      notify: false,
      ghostMode: {
        clicks: true,
        forms: true,
        scroll: false,
      },
    },
    callback,
  );
};

export { asciidoctorServer };
