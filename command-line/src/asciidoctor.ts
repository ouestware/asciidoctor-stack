import { exec } from "child_process";
import * as util from "util";
import * as fs from "fs";
import * as path from "path";
import { scss } from "./stylesheet";
import Processor from "asciidoctor";
import kroki from "asciidoctor-kroki";
import chart from "asciidoctor-chart";
import { getOption } from "./utils";

const execAsync = util.promisify(exec);

// Asciidotor initialization
// ---------------------------
const processor = Processor();

// Register Kroki extension ( for graphviz )
kroki.register(processor.Extensions);
// Register chart extension
chart.register(processor.Extensions);

export interface Option {
  name: string;
  value: string | number;
}

/**
 * Execute asciidoctor command.
 * @param file {string} the asciidoctor file to transform
 * @param backendPath {string} Path to the asciidoctor backend path
 */
const asciidoctor = async (file: string, backendPath: string, options: Array<Option> = []): Promise<void> => {
  try {
    // Testing if file exist and that it is not empty
    await fs.promises.access(file);
  } catch (error) {
    throw new Error("File doesn't exist");
  }

  // Compile sass file if needed
  try {
    const stylesdir: string = getOption<string>(options, "stylesdir", path.resolve(__dirname + "./../../themes/"));
    const stylesheet: string = getOption<string>(options, "stylesheet", "index.css");
    const sassFile = `${stylesdir}/${stylesheet.replace(/.css$/i, ".scss")}`;
    await fs.promises.access(sassFile);
    await scss(sassFile);
  } catch (e) {}

  try {
    // Transform adoc content into html
    const html = processor.convertFile(file, {
      attributes: options.map(item => (item.value !== "" ? `${item.name}=${item.value}` : `${item.name}`)),
      safe: "unsafe",
      template_dirs: [backendPath],
      template_cache: false,
    });
  } catch (e) {
    throw new Error(`Failed to generate adoc file : ${e.message}`);
  }
};

export { asciidoctor };
