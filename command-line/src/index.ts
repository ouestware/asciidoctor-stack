import { Command, flags } from "@oclif/command";
import * as path from "path";
import { printPDF } from "./print-pdf";
import { asciidoctor, Option } from "./asciidoctor";
import { asciidoctorServer } from "./asciidoctor-server";

class AsciidoctorCli extends Command {
  static description = "Generate your asciidoctor file";

  static flags = {
    // display the version
    version: flags.version({ char: "v" }),
    // display the help
    help: flags.help(),
    // specify the theme
    theme: flags.string({ char: "t", description: "Path to the theme folder" }),
    // specify the mode (ie. slides or doc )
    slides: flags.boolean({ char: "s", description: "Generates slides" }),
    // specify if we want a pdf
    pdf: flags.boolean({ description: "Generating PDF", exclusive: ["server"] }),
    // PDF page size settings for slides
    // If not set we use the screen resolution 1920x1080
    width: flags.string({ char: "w", description: "Width PDF page", dependsOn: ["pdf", "slides"] }),
    height: flags.string({ char: "h", description: "Height PDF page", dependsOn: ["pdf", "slides"] }),
    // specify if we need to serve run the server
    server: flags.boolean({
      description: "Run a web server with auto-reload features",
      default: false,
      exclusive: ["pdf"],
    }),
  };

  static args = [{ name: "file", required: true }];

  async run(): Promise<void> {
    // Checking inputs Command
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    const { args, flags } = this.parse(AsciidoctorCli);

    // Checking flags
    const slides: boolean = flags.slides;
    const theme: string = flags.theme ? path.resolve(flags.theme) : undefined;
    const server: boolean = flags.server;
    const pdf: boolean = flags.pdf;

    // Checking args
    const file: string = path.resolve(args.file); // TODO: Check the path to see if it exist

    // Computing script variabels
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    let mode = "doc";
    if (slides) {
      mode = "slides";
    }

    // Computing the adoc backend path to use
    // default one is docs
    let backendPath: string = path.resolve(__dirname, "./../../backends/docs");
    if (mode === "slides") {
      backendPath = path.resolve(__dirname, "./../../backends/slides");
    }

    // Computing adoc options
    // For simplicity, I put all available options, even if it's not needed.
    const options: Array<Option> = [
      { name: "revealjs-dir", value: path.resolve(__dirname, "./../node_modules/reveal.js/dist") },
      { name: "chartist-dir", value: path.resolve(__dirname, "./../node_modules/chartist/dist") },
      { name: "codemirror-dir", value: path.resolve(__dirname, "./../node_modules/codemirror") },
      { name: "codemirror-graphql-dir", value: path.resolve(__dirname, "./../node_modules/codemirror-graphql") },
      { name: "source-highlighter", value: "codemirror" },
      { name: "allow-uri-read", value: "" },
      { name: "sectnums", value: "" },
      { name: "icons", value: "font" },
    ];
    if (theme) {
      options.push({ name: "stylesdir", value: theme }, { name: "stylesheet", value: "index.css" });
    } else {
      options.push(
        { name: "stylesdir", value: path.resolve(__dirname, "./../../themes") },
        { name: "stylesheet", value: "asciidoctor.css" },
      );
    }

    // Let's do the work of the command
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if (!pdf) {
      // if no server, then it's juts calling adoc
      if (!server) {
        try {
          await asciidoctor(file, backendPath, options);
        } catch (e) {
          console.log(e);
        }
      } else {
        try {
          await asciidoctorServer(file, backendPath, options);
        } catch (e) {
          console.log(e);
        }
      }
    } else {
      await asciidoctorServer(file, backendPath, options, async (err, server) => {
        try {
          await printPDF(
            `http://localhost:${server.getOption("port")}/${path.basename(file).replace(/.adoc$/i, ".html")}?print-pdf`,
            path.basename(file).replace(/.adoc$/i, ".pdf"),
          );
          process.exit();
        } catch (e) {
          console.log(e);
        }
      });
    }
  }
}

export = AsciidoctorCli;
