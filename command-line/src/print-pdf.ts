import puppeteer from "puppeteer";

const printPDF = async (url: string, file: string): Promise<void> => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });
  await page.pdf({ path: file, printBackground: true, preferCSSPageSize: true });
  await browser.close();
  return;
};

export { printPDF };
