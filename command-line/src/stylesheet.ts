import * as sass from "node-sass";
import * as util from "util";
import * as path from "path";
import * as fs from "fs";

const renderAsync = util.promisify(sass.render);

/**
 * Function that compiles SASS file in CSS.
 * @param file {string} The file to transform
 */
const scss = async (file: string): Promise<void> => {
  const content: string = await fs.promises.readFile(file, { encoding: "utf-8" });
  if (!content) {
    throw new Error("File is empty");
  }
  try {
    const result = await renderAsync({ file: file });
    await await fs.promises.writeFile(
      path.resolve(path.dirname(file), "./", path.basename(file).replace(/.scss$/i, ".css")),
      result.css,
    );
  } catch (e) {
    throw e;
  }
  return;
};

export { scss };
