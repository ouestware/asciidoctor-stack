import { Option } from "./asciidoctor";

export function getOption<T extends string | number>(options: Array<Option>, name: string, defaultValue: T): T {
  return (options.filter(opt => opt.name === name).pop().value as T) || (defaultValue as T);
}
