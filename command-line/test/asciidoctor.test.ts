import * as assert from "assert";
import * as path from "path";
import { asciidoctor } from "../src/asciidoctor";

const adoc_slides_template: string = path.resolve(__dirname, "./../../backends/slides");
const adoc_docs_template: string = path.resolve(__dirname, "./../../backends/docs");

describe("Testing Asciidoctor", () => {
  it("Generating valid  adoc should work", async () => {
    await asciidoctor(path.resolve(__dirname, "./assets/adoc/test.valid.adoc"), adoc_docs_template);
  });
  it("Generating valid slides adoc should work", async () => {
    await asciidoctor(path.resolve(__dirname, "./assets/adoc/test.valid.adoc"), adoc_slides_template);
  });
  // it("Generating with invalid adoc should fail", async () => {
  //   await assert.rejects(
  //     asciidoctor(path.resolve(__dirname, "./assets/adoc/test.invalid.adoc"), adoc_slides_template),
  //     {
  //       name: "Error",
  //     },
  //   );
  // });
  it("Generating a non-existing file should fail", async () => {
    await assert.rejects(asciidoctor(path.resolve(__dirname, "./QWERTY.adoc"), adoc_slides_template));
  });
});
