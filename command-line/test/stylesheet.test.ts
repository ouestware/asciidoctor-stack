import * as assert from "assert";
import * as path from "path";
import { scss } from "../src/stylesheet";

const adoc_slides_template = "../../backends/slides";
const adoc_docs_template = "../../backends/docs";

describe("Testing sass", () => {
  it("Generating valid sass should work", async () => {
    await scss(path.resolve("./test/assets/sass/test.valid.scss"));
  });
  it("Generating empty sass file should fail", async () => {
    await assert.rejects(scss(path.resolve("./test/assets/sass/test.empty.scss")), {
      name: "Error",
      message: "File is empty",
    });
  });
  it("Generating invalid sass should fail", async () => {
    await assert.rejects(scss(path.resolve("./test/assets/sass/test.invalid.scss")), {
      name: "Error",
      message: 'Invalid CSS after "...ze: $font-size;": expected "}", was ""',
    });
  });
  it("Generating a non-existing file should fail", async () => {
    await assert.rejects(scss(path.resolve("./QWERTY.scss")));
  });
});
